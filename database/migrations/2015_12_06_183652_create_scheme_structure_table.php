<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchemeStructureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheme_structure', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scheme_id');
            $table->integer('subject_type_id')->unsigned();
            $table->foreign('subject_type_id')->references('id')->on('value');
            $table->integer('max_lecture_credit')->default(0);
            $table->integer('max_tutorial_credit')->default(0);
            $table->integer('max_practical_credit')->default(0);
            $table->integer('max_other_credits')->default(0);//This can be used for seminar / project / viva. For Projects this will be calculated field from table project_work_cre
            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->integer('updated_by')->unsigned();
            $table->foreign('updated_by')->references('id')->on('users');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
