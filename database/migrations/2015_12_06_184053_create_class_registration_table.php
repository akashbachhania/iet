<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassRegistrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_registration', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('session');
            $table->integer('section_id')->unsigned();
            $table->foreign('section_id')->references('id')->on('section');
            $table->integer('student_id')->unsigned();
            $table->foreign('student_id')->references('id')->on('student');
            $table->integer('scheme_id')->unsigned();
            $table->foreign('scheme_id')->references('id')->on('scheme');
            $table->date('registration_date');
            $table->integer('group_id')->unsigned();//Refer to Key value table. Key can be "Class Groups"
            $table->foreign('group_id')->references('id')->on('value');
            $table->integer('student_status_id')->unsigned();//Refer to Key value table. Key can be "Student Status"
            $table->foreign('student_status_id')->references('id')->on('value');
            $table->string('roll_no', 10);
            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->integer('updated_by')->unsigned();
            $table->foreign('updated_by')->references('id')->on('users');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_registration');
    }
}
