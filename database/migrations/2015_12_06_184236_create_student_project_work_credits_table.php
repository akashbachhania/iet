<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentProjectWorkCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_project_work_credit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id')->unsigned();
            $table->foreign('student_id')->references('id')->on('student');
            $table->integer('student_scheme_subject_id')->unsigned();
            $table->foreign('student_scheme_subject_id')->references('id')->on('student_scheme_subject');
            $table->integer('valution_type_id')->unsigned();//Refer to Key value table. Key can be "Valuation Type"
            $table->foreign('valution_type_id')->references('id')->on('value');
            $table->integer('supervisor_credits')->nullable();
            $table->integer('expert_credits');//If valuation type is Internal, then internal expert credits else external expert credits
            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->integer('updated_by')->unsigned();
            $table->foreign('updated_by')->references('id')->on('users');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('student_project_work_credit');
    }
}
