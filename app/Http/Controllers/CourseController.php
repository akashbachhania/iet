<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Course;
use App\Key;
use App\Value;
class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = $request->all();
        if($result)
        { 
            $courseTab = new Course;
            $courseTab->code = $result['course_code'];
            $courseTab->name = $result['course_name'];
            if(isset($result['course_description']))
            {
             $courseTab->description = $result['course_description'];
            }
            else
            {
                $courseTab->description = null;
            }
            $courseTab->course_type_id = Value::where('title', $result['course'])->pluck('key_id');
            $courseTab->no_of_terms = $result['terms'];
            $courseTab->min_no_of_terms = 0;
            $courseTab->min_no_of_credits = 0;
            $courseTab->min_GGPA = 0;
            $courseTab->min_SGPA = 0; 
            $courseTab->min_comlpetion_period = 0;
            $courseTab->max_completion_period = 0;
            $courseTab->no_of_core_elective_subjects =0;
            $courseTab->no_of_laboratory = 0;
            $courseTab->no_project_work = 0;
            $courseTab->status = 1;
            $courseTab->is_deleted = 0;
            $courseTab->created_by = 1;
            $courseTab->updated_by = 1;
            $courseTab->created_at = time();
            $courseTab->updated_at = time();
            $courseTab->save();
        }
        
    }
    //Function to get all the course 
    public function getAllCourses()
    {
        $allCourse = Course::get();
        return $allCourse;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
