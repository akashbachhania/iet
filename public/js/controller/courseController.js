angular.module('davv').controller('courseController', ['$scope', '$http', function ($scope, $http) {
	
	$scope.results = [];
	$scope.data = [];
	$scope.getAllCourses = function () {
		console.log('called getAllCourses');
		$http({
			url:'/getAllCourses',
			method : 'GET'
		}).success(function(response){
			console.log(response);
		
			angular.forEach(response, function(value){
				$scope.data = [];
				$scope.data.course_code = value['code'];
				$scope.data.course_name = value['name'];
				$scope.data.course = value['course_type_id'];
				$scope.data.terms = value['terms'];
				$scope.results.push($scope.data);
			});

			// $scope.results.push($scope.data);
		})
	};
	$scope.getAllCourses();
	$scope.storeData = function()
	{
		var data = {
			course_code : $scope.course_code,
			course_name : $scope.course_name,
			course_description : $scope.course_description,
			course : $scope.course,
			terms : $scope.terms,
		};
		console.log(data);
		$http({
			url : '/storeData',
			method : "GET",
			params : data,
		}).success(function(response){
			$scope.results.push(data);
		});
	};
	$scope.data = [];
	$scope.getAllCourses = function () {
		$http({
			url:'/getAllCourses',
			method : 'GET'
		}).success(function(response){

			angular.forEach(response, function(value){
				$scope.data.course_code = value['course_code'];
				$scope.data.course_name = value['course_name'];
				$scope.data.course = value['course'];
				$scope.data.terms = value['terms'];
			});

			$scope.results.push($scope.data);
		})
	};
}])