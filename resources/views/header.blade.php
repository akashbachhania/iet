<!DOCTYPE html>

<html>

<head>

  <title>Exam System</title>

  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="/css/main.css">
</head>
<body ng-app="davv">

  <nav class="navbar">
    <div class="container-fluid">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>  
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="collapse-1">
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Courses <span class="caret"></span></a>

                <ul class="dropdown-menu">
                    <li><a href="http://devmis.new.flair-solution.com/Exam_System/form.html">Form</a></li>
                    <li><a href="http://devmis.new.flair-solution.com/Exam_System/form2.html">Form2</a></li>
                    <li><a href="http://devmis.new.flair-solution.com/Exam_System/form3.html">Form3</a></li>
                    <li><a href="http://devmis.new.flair-solution.com/Exam_System/form4.html">Form4</a></li>
                  </ul>
              </li>
            </ul>
        </div><!-- /.navbar-collapse -->

      </div><!-- /.container-fluid -->
  </nav>
