@extends('master')
@section('content')
<div class="container-fluid" ng-controller="courseController" >
    <div class="row" >
      <div class="col-md-4 col-md-push-1 table_form">
        <form>
          <table class="table table-bordered">
            <thead>
              <th>Code</th>
              <th>Course Name</th>
              <th>Year/Semester</th>
              <th>No. of terms</th>
            </thead>

            <tbody >
              <tr ng-repeat="data in results track by $index">
                <td><%data.course_code%></td>
                <td><input type="text" placeholder="B.E" ng-model="data.course_name"></td>
                <td><input type="text" placeholder="S" ng-model="data.course"></td>
                <td><input type="text" placeholder="B" ng-model="data.terms"></td>
              </tr>

             <!--  <tr>
                <td>1</td>
                <td><input type="text" placeholder="B.E"></td>
                <td><input type="text" placeholder="S"></td>
                <td><input type="text" placeholder="B"></td>
              </tr>
              
              <tr>
                <td>1</td>
                <td><input type="text" placeholder="B.E"></td>
                <td><input type="text" placeholder="S"></td>
                <td><input type="text" placeholder="B"></td>
              </tr>

            <tr>
              <td>1</td>
              <td><input type="text" placeholder="B.E"></td>
              <td><input type="text" placeholder="S"></td>
              <td><input type="text" placeholder="B"></td>
            </tr> -->
          </tbody>
        </table>
      </form>
    </div>


    <div class="col-md-3 col-md-push-2">
      <form >
      <fieldset ng-form="courseEntry">
	        <div class="form-group">
	            <label for="course_code">Course Code</label>
	            <input type="text" ng-model="course_code" class="form-control" id="course_code" placeholder="Course Code" >
	        	{{$errors->first('course_code', '<span class="help-block"></span>')}}



	        </div>
        
	        <div class="form-group">
	            <label for="course_name">Course Name</label>
	            <input type="text" class="form-control" ng-model="course_name" id="course_name" placeholder="course_name" >
	        </div>
        
          <div class="form-group">
            <label for="course_description">Course Description</label>
            <textarea class="form-control" name="course_description" ng-model="course_description" placeholder="Course Description" ></textarea>
          </div>

          <div class="row"> 
              <h4 class=" text-center">Course Yearly/Semester</h4>    
        
            <div class="col-md-4 text-center check">
	             <label>
	                <input type="radio" ng-model="course" value="year" name="course"> Year
	             </label>
            </div>

            <div class="col-md-4 text-center">
               <label>
                <input type="radio" ng-model="course" value="semester" name="course"> Semester
               </label>
           </div>
          </div>

          <div class="form-group terms">
            <label for="terms">No. of terms</label>
              <input type="text"  class="form-control" ng-model="terms" id="terms" placeholder="" >
            <label for="terms">Semester</label>         
          </div>

        <button type="button" ng-click="storeData();" class="btn btn-success">Submit</button>
      </fieldset>
      </form>

    </div>

    </div>
  </div>

@endsection